#ifndef SURFACE_H
#define SURFACE_H

#include <qopengl.h>
#include <QVector>
#include <QVector3D>
#include <vector>

struct Vertex3D{
    GLfloat x;
    GLfloat y;
    GLfloat z;
};

class Surface{
public:
	Surface();
	
	const GLfloat *constData() const { return m_data.constData();}
	int count() const { return m_count;}
	int vertexCount() const { return m_count / 6;}
	
	GLfloat getWidth() const { return width;}
	GLfloat getHeight() const { return height;}
	GLfloat getXRef() const {return xref;}
	GLfloat getYRef() const {return yref;}
	GLfloat getZRef() const {return zref;}

	unsigned int len;


protected:
	void generateHeightMap();

private:
	void quad(GLfloat x1, GLfloat y1, GLfloat z1, GLfloat x2, GLfloat y2, GLfloat z2, GLfloat x3, GLfloat y3, GLfloat z3, GLfloat x4, GLfloat y4, GLfloat z4);
	void triangle(GLfloat x1, GLfloat y1, GLfloat z1, GLfloat x2, GLfloat y2, GLfloat z2, GLfloat x3, GLfloat y3, GLfloat z3);
	void add(const QVector3D &v, const QVector3D &n);

	QVector<GLfloat> m_data;
	std::vector<float> HMColor;
	int m_count;

	GLfloat width;
	GLfloat height;
	GLfloat xref;
	GLfloat yref;
	GLfloat zref;

	GLfloat zmax;

	

	int HMwidth, HMheight, HMnrChannels;
    unsigned char *HMdata;
};

#endif // SURFACE_H