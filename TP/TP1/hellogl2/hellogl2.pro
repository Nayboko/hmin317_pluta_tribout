HEADERS       = glwidget.h \
                window.h \
                mainwindow.h \
                logo.h \
                surface.hpp \
                stb_image.h
SOURCES       = glwidget.cpp \
                main.cpp \
                window.cpp \
                mainwindow.cpp \
                logo.cpp \
                surface.cpp \
                stb_image.cpp

QT           += widgets
QT           += opengl

# install
target.path = $$[QT_INSTALL_EXAMPLES]/opengl/hellogl2
INSTALLS += target
