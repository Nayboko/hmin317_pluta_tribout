#include "surface.hpp"
#include <qmath.h>
#include <QImage>
#include <QColor>
#include <vector>
#include <iostream>
#include <time.h> 



Surface::Surface() : len(256), m_count(0), width(0.50f), height(0.50f), xref(0.00f), yref(0.00f), zref(0.00f), zmax(0.05f){

     // Coordonnées des vertices
    std::vector<Vertex3D> vhandler;
	// 16 sommets car 0 -> 15
    unsigned int parcours = len * (len - 1) - 1;
    float pasx(width/(float)len), pasy(height/(float)len);

	this->generateHeightMap();

    // Création des coords des vertex en XijYij
    for(unsigned int i = 0; i < len; ++i){
    	for(unsigned int j = 0; j < len; ++j){
    		GLfloat xij = i * pasx + xref;
    		GLfloat yij = j * pasy + yref;
            //GLfloat zij = (((float)((rand()% 5) - 1)))/100.0f + zref;
			//GLfloat zij = zref;
			GLfloat zij = HMColor.at(i*len+j);
			//std::cerr << "zij = " << zij << std::endl;
            
            Vertex3D v; v.x = xij; v.y = yij; v.z = zij;
            vhandler.push_back(v);
    		//std::cerr << "[" << i << ":" << j <<"] =" << "x= " << xij << " y= " << yij << " z = " << zij << std::endl;
    	}
    }

std::cout << vhandler.size() << ", "<< parcours << std::endl;
    GLfloat x1, x2, x3, x4, y1, y2, y3, y4, z1, z2, z3, z4;
    Vertex3D v1, v2, v3, v4;
    for(unsigned int i = 0; i < parcours; ++i){
     //   std::cout << i + len +1 << std::endl;
        v1 = vhandler[i]; x1 = v1.x; y1 = v1.y; z1 = v1.z;
        v2 = vhandler[i+1]; x2 = v2.x; y2 = v2.y; z2 = v2.z;
        v3 = vhandler[i+len]; x3 = v3.x; y3 = v3.y; z3 = v3.z;
        v4 = vhandler[i + len +1]; x4 = v4.x; y4 = v4.y; z4 = v4.z;
/*
        std::cerr << "v1 : (" << v1.x << ", " << v1.y << ")" << std::endl;
        std::cerr << "v2 : (" << v2.x << ", " << v2.y << ")" << std::endl;
        std::cerr << "v3 : (" << v3.x << ", " << v3.y << ")" << std::endl;
        std::cerr << "v4 : (" << v4.x << ", " << v4.y << ")" << std::endl;*/

        triangle(x1, y1, z1, x2, y2, z2, x3, y3, z3);
        triangle(x4, y4, z4, x3, y3, z3, x2, y2, z2);
    }
    // std::cout << "youpi" << std::endl;
}

void Surface::generateHeightMap()
{
	HMColor.clear();
	QImage img = QImage(QString("./Heightmap.png"));
	//QImage img = QImage(QString("./ground.bmp"));
	//QImage img = QImage(QString("./heightmap2.png"));
	if (!img.isNull()) {
		QRgb color;
		int vertices_by_x = img.width();
		int vertices_by_y = img.height();

		unsigned int pasX = vertices_by_x / this->len;
		unsigned int pasY = vertices_by_y / this->len;

		for (unsigned int y = 0; y < pasY * this->len; ++y) {
			for (unsigned int x = 0; x < pasX * this->len; ++x) {
				color = img.pixel(x, y);
				this->HMColor.push_back((static_cast<float>(qGray(color)) / 255.00f)* zmax);
			}
		}
	}	
}

void Surface::quad(GLfloat x1, GLfloat y1, GLfloat z1, GLfloat x2, GLfloat y2, GLfloat z2, GLfloat x3, GLfloat y3, GLfloat z3, GLfloat x4, GLfloat y4, GLfloat z4)
{
    QVector3D n = QVector3D::normal(QVector3D(x4 - x1, y4 - y1, 0.0f), QVector3D(x2 - x1, y2 - y1, 0.0f));

    add(QVector3D(x1, y1, z1), n); // x - y - z d'un vertex
    add(QVector3D(x4, y4, z4), n);
    add(QVector3D(x2, y2, z2), n);

    add(QVector3D(x3, y3, z3), n);
    add(QVector3D(x2, y2, z2), n);
    add(QVector3D(x4, y4, z4), n);

    n = QVector3D::normal(QVector3D(x1 - x4, y1 - y4, 0.0f), QVector3D(x2 - x4, y2 - y4, 0.0f));

    add(QVector3D(x4, y4, z4), n);
    add(QVector3D(x1, y1, z1), n);
    add(QVector3D(x2, y2, z2), n);

    add(QVector3D(x2, y2, z2), n);
    add(QVector3D(x3, y3, z3), n);
    add(QVector3D(x4, y4, z4), n);

}

void Surface::triangle(GLfloat x1, GLfloat y1, GLfloat z1, GLfloat x2, GLfloat y2, GLfloat z2, GLfloat x3, GLfloat y3, GLfloat z3){
	QVector3D n = QVector3D::normal(QVector3D(x2 - x1, y2 - y1, z2 - z1), QVector3D(x3 - x1, y3 - y1, z3 - z1));

	add(QVector3D(x1, y1, z1), n);
	add(QVector3D(x2, y2, z2), n);
	add(QVector3D(x3, y3, z3), n);

	n = QVector3D::normal(QVector3D(x1 - x2, y1 - y2, z1 - z2), QVector3D(x1 - x3, y1 - y3, z1 - z3));

	add(QVector3D(x3, y3, z3), n);
	add(QVector3D(x1, y1, z1), n);
	add(QVector3D(x2, y2, z2), n);

}

void Surface::add(const QVector3D &v, const QVector3D &n)
{
    m_data.push_back(v.x()); ++m_count;
    m_data.push_back(v.y()); ++m_count;
    m_data.push_back(v.z()); ++m_count;
    m_data.push_back(n.x()); ++m_count;
    m_data.push_back(n.y()); ++m_count;
    m_data.push_back(n.z()); ++m_count;
}