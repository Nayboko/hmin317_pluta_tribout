#include "glwidget.h"
#include <QMouseEvent>
#include <QOpenGLShaderProgram>
#include <QCoreApplication>
#include <GLFW/glfw3.h>
#include <math.h>
#include <QGLWidget>
#include <QGLShaderProgram>
#include <QGLContext>
#include <QElapsedTimer>
#include <QTimer>
#include <iostream>
#include "stb_image.h"

bool GLWidget::m_transparent = false;

GLWidget::GLWidget(QWidget *parent)
    : QOpenGLWidget(parent),
	  nbFPS(30),
	  m_xRot(0),
      m_yRot(0),
      m_zRot(0),
      m_program(0)
{
    m_core = QSurfaceFormat::defaultFormat().profile() == QSurfaceFormat::CoreProfile;
    // --transparent causes the clear color to be transparent. Therefore, on systems that
    // support it, the widget will become transparent apart from the logo.
    if (m_transparent) {
        QSurfaceFormat fmt = format();
        fmt.setAlphaBufferSize(8);
        setFormat(fmt);
    }
	setFocusPolicy(Qt::StrongFocus); // Force le focus sur le widget pour les events

}

GLWidget::~GLWidget()
{
    cleanup();
}

QSize GLWidget::minimumSizeHint() const
{
    return QSize(50, 50);
}

QSize GLWidget::sizeHint() const
{
    return QSize(400, 400);
}

static void qNormalizeAngle(int &angle)
{
    while (angle < 0)
        angle += 360 * 16;
    while (angle > 360 * 16)
        angle -= 360 * 16;
}

void GLWidget::setXRotation(int angle)
{
    qNormalizeAngle(angle);
    if (angle != m_xRot) {
        m_xRot = angle;
        emit xRotationChanged(angle);
        update();
    }
}

void GLWidget::setYRotation(int angle)
{
    qNormalizeAngle(angle);
    if (angle != m_yRot) {
        m_yRot = angle;
        emit yRotationChanged(angle);
        update();
    }
}

void GLWidget::setZRotation(int angle)
{
    qNormalizeAngle(angle);
    if (angle != m_zRot) {
        m_zRot = angle;
        emit zRotationChanged(angle);
        update();
    }
}

void GLWidget::cleanup()
{
    if (m_program == nullptr)
        return;
    makeCurrent();
	m_surfaceVbo.destroy();
    //m_logoVbo.destroy();
    delete m_program;
    m_program = 0;
    doneCurrent();
}

void GLWidget::renderLater()
{
	update();
}

static const char *vertexShaderSourceCore =
    "#version 150\n"
    "in vec4 vertex;\n"
    "in vec3 normal;\n"
    //"in vec2 textureCoordinate;\n"
    "layout (location = 2) in vec2 aTexCoord;\n"
    "out vec3 vert;\n"
    "out vec3 vertNormal;\n"
    //"out vec2 varyingTextureCoordinate;\n"
    "out vec2 TexCoord; \n"
    "uniform mat4 projMatrix;\n"
    "uniform mat4 mvMatrix;\n"
    "uniform mat3 normalMatrix;\n"
    "void main() {\n"
    "   vert = vertex.xyz;\n"
    "   vertNormal = normalMatrix * normal;\n"
    //"   varyingTextureCoordinate = textureCoordinate; \n"
    "   TexCoord = aTexCoord;\n"
    "   gl_Position = projMatrix * mvMatrix * vertex;\n"
    "}\n";

static const char *fragmentShaderSourceCore =
    "#version 150\n"
    "in highp vec3 vert;\n"
    "in highp vec3 vertNormal;\n"
    "in vec2 TexCoord; \n"
    "out highp vec4 fragColor;\n"
    "uniform highp vec3 lightPos;\n"
    "uniform sampler2D ourTexture; \n"
    "void main() {\n"
    "   highp vec3 L = normalize(lightPos - vert);\n"
    "   highp float NL = max(dot(normalize(vertNormal), L), 0.0);\n"
    "   highp vec3 color = vec3(173./255., 216./255., 230./255.);\n"
    "   highp vec3 col = clamp(color * 0.2 + color * 0.8 * NL, 0.0, 1.0);\n"
    //"   fragColor = vec4(col, 1.0);\n"
    //"   fragColor = texture2D(texture, varyingTextureCoordinate); \n"
    "   fragColor = texture(ourTexture, TexCoord);\n"
    "}\n";

static const char *vertexShaderSource =
    "attribute vec4 vertex;\n"
    "attribute vec3 normal;\n"
    "attribute vec2 aTexCoord;\n"
    "varying vec3 vert;\n"
    "varying vec3 vertNormal;\n"
    "varying vec2 TexCoord;\n"
    "uniform mat4 projMatrix;\n"
    "uniform mat4 mvMatrix;\n"
    "uniform mat3 normalMatrix;\n"
    "void main() {\n"
    "   vert = vertex.xyz;\n"
    "   vertNormal = normalMatrix * normal;\n"
    "   gl_Position = projMatrix * mvMatrix * vertex;\n"
    "   TexCoord = aTexCoord; \n"
    "}\n";

static const char *fragmentShaderSource =
    "varying highp vec3 vert;\n"
    "varying highp vec3 vertNormal;\n"
    "varying highp vec2 TexCoord; \n"
    "uniform highp vec3 lightPos;\n"
    "uniform highp sampler2D ourTexture; \n"
    "void main() {\n"
    "   highp vec3 L = normalize(lightPos - vert);\n"
    "   highp float NL = max(dot(normalize(vertNormal), L), 0.0);\n"
    "   highp vec3 color = vec3(205./255., 133./255., 63./255.);\n"
    "   highp vec3 col = clamp(color * 0.2 + color * 0.8 * NL, 0.0, 1.0);\n"
    //"   gl_FragColor = vec4(col, 1.0);\n"
    "   gl_FragColor = texture2D(ourTexture,TexCoord)  * vec4(col, 1.0);\n"
    "}\n";

void GLWidget::initializeGL()
{
    // In this example the widget's corresponding top-level window can change
    // several times during the widget's lifetime. Whenever this happens, the
    // QOpenGLWidget's associated context is destroyed and a new one is created.
    // Therefore we have to be prepared to clean up the resources on the
    // aboutToBeDestroyed() signal, instead of the destructor. The emission of
    // the signal will be followed by an invocation of initializeGL() where we
    // can recreate all resources.

	// Fixe un update de l'affichage à 30 fps
	QTimer* timer = new QTimer();
	connect(timer, SIGNAL(timeout()), this, SLOT(renderLater()));
	timer->start(1000 / nbFPS);
	previousNanosec = 0;
	nextNanosec = 1;
	


    connect(context(), &QOpenGLContext::aboutToBeDestroyed, this, &GLWidget::cleanup);

    initializeOpenGLFunctions();
    glClearColor(0, 0, 0, m_transparent ? 0 : 1);

    m_program = new QOpenGLShaderProgram;
    m_program->addShaderFromSourceCode(QOpenGLShader::Vertex, m_core ? vertexShaderSourceCore : vertexShaderSource);
    m_program->addShaderFromSourceCode(QOpenGLShader::Fragment, m_core ? fragmentShaderSourceCore : fragmentShaderSource);
    m_program->bindAttributeLocation("vertex", 0);
    m_program->bindAttributeLocation("normal", 1);
    m_program->link();

    m_program->bind();
    m_projMatrixLoc = m_program->uniformLocation("projMatrix");
    m_mvMatrixLoc = m_program->uniformLocation("mvMatrix");
    m_normalMatrixLoc = m_program->uniformLocation("normalMatrix");
    m_lightPosLoc = m_program->uniformLocation("lightPos");

    // Create a vertex array object. In OpenGL ES 2.0 and OpenGL 2.x
    // implementations this is optional and support may not be present
    // at all. Nonetheless the below code works in all cases and makes
    // sure there is a VAO when one is needed.
    m_vao.create();
    QOpenGLVertexArrayObject::Binder vaoBinder(&m_vao);

    // Setup our vertex buffer object.
    /*m_logoVbo.create();
    m_logoVbo.bind();
    m_logoVbo.allocate(m_logo.constData(), m_logo.count() * sizeof(GLfloat));*/
    m_surfaceVbo.create();
    m_surfaceVbo.bind();
    m_surfaceVbo.allocate(m_surface.constData(), m_surface.count() * sizeof(GLfloat));

    std::cerr << "m_surface.count : " << m_surface.count() << std::endl;

    // Store the vertex attribute bindings for the program.
    setupVertexAttribs();

    // Our camera never changes in this example.
    m_camera.setToIdentity();
    m_camera.translate(m_surface.getXRef()-(m_surface.getWidth()/2), m_surface.getYRef()+(m_surface.getHeight()/2), -1.5f);

    // Light position is fixed.
    m_program->setUniformValue(m_lightPosLoc, QVector3D(0, 0, 70));

    // texture coordinates
    //textureCoordinates << QVector2D(0, 1) << QVector2D(1,1) << QVector2D(0,0);
    //glGenTextures(2, &texture);
    //glBindTexture(GL_TEXTURE_2D, texture); 
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    int width, height, nrChannels;
    unsigned char *data = stbi_load("ground.bmp", &width, &height, &nrChannels, 0);
    if(data){
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
    }else{
        std::cout << "Failed to load texture" << std::endl;
    }

    stbi_image_free(data); // Libération image

    m_program->release();
}

void GLWidget::setupVertexAttribs()
{
    /*m_logoVbo.bind();
    QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();
    f->glEnableVertexAttribArray(0);
    f->glEnableVertexAttribArray(1);
    f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), 0);
    f->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), reinterpret_cast<void *>(3 * sizeof(GLfloat)));
    m_logoVbo.release();*/
    m_surfaceVbo.bind();
    QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();
    f->glEnableVertexAttribArray(0);
    f->glEnableVertexAttribArray(1);
    f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), 0);
    f->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), reinterpret_cast<void *>(3 * sizeof(GLfloat)));
    m_surfaceVbo.release();
}

// Boucle de réaffichage
void GLWidget::paintGL()
{
	timer.start();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    m_world.setToIdentity();
    m_world.rotate(180.0f - (m_xRot / 16.0f) , 1, 0, 0);
    m_world.rotate((m_yRot / 16.0f) , 0, 1, 0);
    m_world.rotate((m_zRot / 16.0f), 0, 0, 1);

    QOpenGLVertexArrayObject::Binder vaoBinder(&m_vao);
    m_program->bind();
    m_program->setUniformValue(m_projMatrixLoc, m_proj);
    m_program->setUniformValue(m_mvMatrixLoc, m_camera * m_world);
    QMatrix3x3 normalMatrix = m_world.normalMatrix();
    m_program->setUniformValue(m_normalMatrixLoc, normalMatrix);
/*    m_program->setUniformValue("texture", 0);

    glBindTexture(GL_TEXTURE_2D, texture);

    m_program->setAttributeArray("textureCoordinate", textureCoordinates.constData());
    m_program->enableAttributeArray("textureCoordinate");*/

    //glDrawArrays(GL_TRIANGLES, 0, m_logo.vertexCount());
    glBindTexture(GL_TEXTURE_2D, texture);
    glDrawArrays(GL_TRIANGLES, 0, m_surface.vertexCount());
	
	previousNanosec = timer.nsecsElapsed();
    m_program->release();
	nextNanosec = timer.nsecsElapsed();


	
	//std::cerr << "delta time : " << (nextNanosec - previousNanosec)/ 1000000000.0f << std::endl;
	//std::cerr << "time per frame : " << nextNanosec/ 1000000000.0f  << " sec"<< std::endl;
}

void GLWidget::resizeGL(int w, int h)
{
    m_proj.setToIdentity();
    m_proj.perspective(45.0f, GLfloat(w) / h, 0.01f, 100.0f);
}

void GLWidget::mousePressEvent(QMouseEvent *event)
{
    m_lastPos = event->pos();
    event->accept();
}

void GLWidget::mouseMoveEvent(QMouseEvent *event)
{
    int dx = event->x() - m_lastPos.x();
    int dy = event->y() - m_lastPos.y();

    if (event->buttons() & Qt::LeftButton) {
        setXRotation(m_xRot + 8 * dy);
        setYRotation(m_yRot + 8 * dx);
        update();

    } else if (event->buttons() & Qt::RightButton) {
        setXRotation(m_xRot + 8 * dy);
        setZRotation(m_zRot + 8 * dx);
        update();

    }
    m_lastPos = event->pos();
    event->accept();
}

void GLWidget::wheelEvent(QWheelEvent *event)
{
    int delta = event->delta();
    if (event->orientation() == Qt::Vertical) {
        if (delta < 0) {
            //m_zRot *= 1.1;
            m_camera.translate(0.0, 0.0, 0.02);
        } else if (delta > 0) {
            //m_zRot *= 0.9;
            m_camera.translate(0.0, 0.00, -0.02);
        }
    update();
    }
    event->accept();
}


// https://doc.qt.io/qt-5/qkeyevent.html
bool GLWidget::eventFilter(QObject *obj, QEvent *event){
    if(event->type() == QEvent::KeyRelease)
    {
        // Appelle le signal puis:
       /* QKeyEvent *c = dynamic_cast<QKeyEvent *>(event);
        if(c && c->key() == Qt::Key_A)
        {
            std::cerr << "A key pressed" << std::endl;
        }
        if(c && c->key() == Qt::Key_Escape)
        {
            std::cerr << "Echap key pressed" << std::endl;
            QCoreApplication::exit(0);
        }*/
		if (event->type() == QEvent::KeyPress)
		{
			qWarning() << "The bad guy which steals the keyevent is" << obj;
		}
		return false;
    }
    return false;
}

void GLWidget::keyPressEvent(QKeyEvent *event){
    switch(event->key()){
        case Qt::Key::Key_Z:
            m_camera.translate(0.0, 0.01, 0.0);
			//std::cerr << "Z" << std::endl;
        break;
        case Qt::Key::Key_Q:
            m_camera.translate(-0.01, 0.0, 0.0);
			//std::cerr << "Q" << std::endl;
        break;
        case Qt::Key::Key_S:
            m_camera.translate(0.0, -0.01, 0.0);
			//std::cerr << "S" << std::endl;
        break;
        case Qt::Key::Key_D:
            m_camera.translate(0.01, 0.0, 0.0);
			//std::cerr << "D" << std::endl;
        break;
		case Qt::Key::Key_Plus:
			setXRotation(700);
			//std::cerr << "D" << std::endl;
		break;
		case Qt::Key::Key_Minus:
			setXRotation(0);
			//std::cerr << "D" << std::endl;
			break;
		case Qt::Key::Key_Up:
			nbFPS++;
			if (nbFPS > 100) nbFPS = 100;
			break;
		case Qt::Key::Key_Up:
			nbFPS--;
			if (nbFPS <= 0) nbFPS = 1;
			break;
    }
    update();
    event->accept();
}