# HMIN317 - Moteurs de jeux - TP1&2
**Prise en main de Qt Creator, Git et OpenGL ES 3.0**

## Compilation du projet :

Ajouter la ligne *QMAKE_CXXFLAGS += -g -Wall -Wextra -std=c++11* au fichier *hellogl2.pro*.

Installer le compilateur QMake qui permettra de générer un MakeFile pour le projet: 

	sudo apt install qtchooser
	sudo apt-get install qt5-qmake
	sudo apt-get install qt5-default

Puis exécuter la commande `qmake`, en précisant la version de qt utilisée, dans le dossier du projet :

	qmake -qt=qt5 .

## Question 1a : À quoi sert la classe GLWidget ?

La classe GLWidget permet de gérer le rendu graphique avec OpenGL. Elle fournit des méthodes publiques pour gérer l'affichage des événements communiqués sous forme de signaux ou de slots aux autres widgets ou objets connectés.

## Question 1b : À quoi servent les shaders? Décrire les opérations effectuées.

Un shader est un petit programme exécuté par la carte graphique codé ici en GLSL (OpenGL Shading Language). Grâce au *parallel processing* extrémement perfomant rendu possible par les GPU (Graphics Processing Unit), il sera exécuté, suivant sa position dans le pipeline 3D, sur la carte graphique.

Dans le code fournit, on retrouvera 2 versions de code source pour chaque shader, une version si on utilise au moins la version 150 de GLSL, et une autre pour les version antérieures de GLSL.

**Vertex Shader** : C'est le premier programme appellé dans le *pipeline 3D* permettant de valider ou de modifier, via des matrices de transformation, la position d'un vertex. **Il est exécuté pour chaque sommet**. En définitive, il va permettre de transformer la forme d'une objet.

**Fragment/Pixel Shader** : C'est un programme qui va permettre de définir la couleur d'un pixel dont la forme est délimité par les sommets reçus en entrée. **Il est exécuté pour chaque pixel dessiné**.


## Question 2a : Expliquer le fonctionnement les méthodes de dessin et de transformation appliquées aux objets.

### Méthode de transformation
```c
	void GLWidget::setXRotation(int angle){
	    qNormalizeAngle(angle);
	    if (angle != m_xRot) {
	        m_xRot = angle;
	        emit xRotationChanged(angle);
	        update();
	    }
	}
```

Une normalisation de l'angle donné en paramètre est effectué, si l'angle de rotation demandé est différent de l'actuel, mets à jour l'angle selon l'axe demandé (ici l'axe X), émét un signal vers un programme MOC (Meta-Object Compiler) permettant de signaler qu'un changement a été effectué :

```c
// SIGNAL 0
void GLWidget::xRotationChanged(int _t1){
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
```

Puis l'affichage est mis à jour.

### Méthode de dessin

Une surcharge de la méthode `void paintGL()` en provenance de la classe `QOpenGLWidget` est effectué de la façon suivante :

```c
	void GLWidget::paintGL(){   
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    // Transformation
    m_world.setToIdentity(); // Revenir à l'origine du repère monde
    m_world.rotate(180.0f - (m_xRot / 16.0f), 1, 0, 0);
    m_world.rotate(m_yRot / 16.0f, 0, 1, 0);
    m_world.rotate(m_zRot / 16.0f, 0, 0, 1);

    // On passe les matrices Shader Handler
    QOpenGLVertexArrayObject::Binder vaoBinder(&m_vao);
    m_program->bind();
    m_program->setUniformValue(m_projMatrixLoc, m_proj);
    m_program->setUniformValue(m_mvMatrixLoc, m_camera * m_world);
    QMatrix3x3 normalMatrix = m_world.normalMatrix();
    m_program->setUniformValue(m_normalMatrixLoc, normalMatrix);

    // On dessine le résultat
    glDrawArrays(GL_TRIANGLES, 0, m_logo.vertexCount());

    // On active le Shader Handler
    m_program->release();
}

```
Dans un premier temps, on initialise les buffers nécessité pour notre scène. Ensuite, on applique les transformations de rotation en X, Y et Z en fonction de la valeur courante des sliders. On bind un Vertex Array Object ainsi que le shader handler dans le `QOpenGLContext`. Par la suite, on configure les variables *uniform* des matrices (Projection, Model View, Normal) à notre Shader Handler `m_program`. On "dessine" le Mesh désiré sous forme de simplex triangulaires et finalement, on appele le Shader Handler pour qu'il puisse calculer notre objet.

## Question 2b : Quelles sont les mécanismes et fonctions permettant de transmettre à l’application les mises à jour des sliders ?

Chaque slider est connecté avec une méthode de la classe `GLWidget` qui sont :
* `setXRotation(int angle)` ;
* `setYRotation(int angle)` ;
* `setZRotation(int angle)` ;


Le lien entre les sliders et les méthodes de transformation est faite de la manière suivante :
```c
	connect(xSlider, &QSlider::valueChanged, glWidget, &GLWidget::setXRotation);
    connect(glWidget, &GLWidget::xRotationChanged, xSlider, &QSlider::setValue);
```

Et à chaque changement effectué sur un slider, la méthode inclus la méthode `update` pour mettre à jour l'affichage.

## Question 3 : Initialiser et afficher une surface plane (16 * 16 sommets) composée de triangles.

On reprend globalement la même trame que celle de la classe `logo` et on redéfinit certaines méthodes dont le constructeur ainsi que `quad` qui devient `triangle`. On pense également à retirer le resize du vector `m_data` et on grandit le dynamiquement dans la méthode `add()`

```c++
Surface::Surface() : len(256), m_count(0), width(0.50f), height(0.50f), xref(0.00f), yref(0.00f), zref(0.00f), zmax(0.05f){

     // Coordonnées des vertices
    std::vector<Vertex3D> vhandler;
    unsigned int parcours = len * (len - 1) - 1;
    float pasx(width/(float)len), pasy(height/(float)len);

    // Création des coords des vertex en XijYij
    for(unsigned int i = 0; i < len; ++i){
    	for(unsigned int j = 0; j < len; ++j){
    		GLfloat xij = i * pasx + xref;
    		GLfloat yij = j * pasy + yref;
			GLfloat zij = zref;
            
            Vertex3D v; v.x = xij; v.y = yij; v.z = zij;
            vhandler.push_back(v);
    	}
    }
	
    // On définit les triangles aux vertices crées précédemment
    GLfloat x1, x2, x3, x4, y1, y2, y3, y4, z1, z2, z3, z4;
    Vertex3D v1, v2, v3, v4;
    for(unsigned int i = 0; i < parcours; ++i){
        v1 = vhandler[i]; x1 = v1.x; y1 = v1.y; z1 = v1.z;
        v2 = vhandler[i+1]; x2 = v2.x; y2 = v2.y; z2 = v2.z;
        v3 = vhandler[i+len]; x3 = v3.x; y3 = v3.y; z3 = v3.z;
        v4 = vhandler[i + len +1]; x4 = v4.x; y4 = v4.y; z4 = v4.z;
        
        triangle(x1, y1, z1, x2, y2, z2, x3, y3, z3);
        triangle(x4, y4, z4, x3, y3, z3, x2, y2, z2);
    }
}
```

```c++
// Méthode d'assemblage des points 3D en triangle
void Surface::triangle(GLfloat x1, GLfloat y1, GLfloat z1, GLfloat x2, GLfloat y2, GLfloat z2, GLfloat x3, GLfloat y3, GLfloat z3){
	QVector3D n = QVector3D::normal(QVector3D(x2 - x1, y2 - y1, z2 - z1), QVector3D(x3 - x1, y3 - y1, z3 - z1));

	add(QVector3D(x1, y1, z1), n);
	add(QVector3D(x2, y2, z2), n);
	add(QVector3D(x3, y3, z3), n);

	n = QVector3D::normal(QVector3D(x1 - x2, y1 - y2, z1 - z2), QVector3D(x1 - x3, y1 - y3, z1 - z3));

	add(QVector3D(x3, y3, z3), n);
	add(QVector3D(x1, y1, z1), n);
	add(QVector3D(x2, y2, z2), n);

}

void Surface::add(const QVector3D &v, const QVector3D &n)
{
    m_data.push_back(v.x()); ++m_count;
    m_data.push_back(v.y()); ++m_count;
    m_data.push_back(v.z()); ++m_count;
    m_data.push_back(n.x()); ++m_count;
    m_data.push_back(n.y()); ++m_count;
    m_data.push_back(n.z()); ++m_count;
}
```

**Résultat :**

<img src="impr/terrain_plat.jpg" alt="Terrain plat" title="Terrain plat" style="zoom:75%;" />

## Question 4a :   Modifier l’altitude (z) des sommets pour réaliser un relief.  

On ajoute :

```c++
Surface::Surface() : len(256), m_count(0), width(0.50f), height(0.50f), xref(0.00f), yref(0.00f), zref(0.00f), zmax(0.05f){
	
    ...
    // GLfloat zij = zref;
	GLfloat zij = (((float)((rand()% 5) - 1)))/100.0f + zref;
    
    ...
        
}
```



**Resultat :**

<img src="impr/terrain_relief.jpg" style="zoom:67%;" title="Terrain avec relief aléatoire" />

## Question 4b :   Utiliser le clavier pour avancer, reculer, et déplacer la caméra de gauche à droite.  

```c++
// Gestion des événements claviers pour les déplacements de la caméra
void GLWidget::keyPressEvent(QKeyEvent *event){
    switch(event->key()){
        case Qt::Key::Key_Z:
            m_camera.translate(0.0, 0.01, 0.0);
        break;
        case Qt::Key::Key_Q:
            m_camera.translate(-0.01, 0.0, 0.0);
        break;
        case Qt::Key::Key_S:
            m_camera.translate(0.0, -0.01, 0.0);
        break;
        case Qt::Key::Key_D:
            m_camera.translate(0.01, 0.0, 0.0);
        break;
		case Qt::Key::Key_Plus:
			setXRotation(700);
		break;
		case Qt::Key::Key_Minus:
			setXRotation(0);
			break;
    }
    update();
    event->accept();
}
```

On pense évidemment à ajouter `glWidget->installEventFilter(this);` dans `window.cpp` pour mettre en place un listener dans le widget `glWidget` ainsi que `setFocusPolicy(Qt::StrongFocus);` dans le constructeur de `GLWidget` pour forcer le focus des listeners sur l'objet widget.



## Question 5 : Modifier votre TP précédent pour lire une carte d’altitude (height map) et afficher un terrain dont l’altitude en chaque point est donnée par la carte fournie. 

On ajoute une méthode permettant de charger une heightmap et de créer un vector contenant la couleur en nuance de gris rapporté sur une échelle entre 0 et `zmax`, cette dernière étant la hauteur maximale autorisée.

```c++
void Surface::generateHeightMap()
{
	HMColor.clear();
	QImage img = QImage(QString("./Heightmap.png"));
	if (!img.isNull()) {
		QRgb color;
		int vertices_by_x = img.width();
		int vertices_by_y = img.height();

		unsigned int pasX = vertices_by_x / this->len;
		unsigned int pasY = vertices_by_y / this->len;

		for (unsigned int y = 0; y < pasY * this->len; ++y) {
			for (unsigned int x = 0; x < pasX * this->len; ++x) {
				color = img.pixel(x, y);
				this->HMColor.push_back((static_cast<float>(qGray(color)) / 255.00f)* zmax);
			}
		}
	}	
}
```

On part sur cette heightmap :

![Heightmap](hellogl2/Heightmap.png)

Et en appliquant de la manière suivante :

```c++
Surface::Surface() : len(256), m_count(0), width(0.50f), height(0.50f), xref(0.00f), yref(0.00f), zref(0.00f), zmax(0.05f){
	
    ...
    for(unsigned int i = 0; i < len; ++i){
    	for(unsigned int j = 0; j < len; ++j){
    		GLfloat xij = i * pasx + xref;
    		GLfloat yij = j * pasy + yref;
            //GLfloat zij = zref;
            //GLfloat zij = (((float)((rand()% 5) - 1)))/100.0f + zref;	
			GLfloat zij = HMColor.at(i*len+j);
        
            Vertex3D v; v.x = xij; v.y = yij; v.z = zij;
            vhandler.push_back(v);
        }
    }
    ...   
}
```

On obtient le résultat suivant :

<img src="impr/terrain_hm.jpg" title="Terrain avec heightmap" style="zoom:75%;" />

## Question 6 : Proposer un autre mode d’affichage pour regarder le terrain sous un angle de 45 degrés  et le faire tourner autour de son origine avec une vitesse constante à l’aide d’un timer. 

N'ayant pas trop vu ce qui était entendu par "autre mode d'affichage", j'ai ajouté dans les contrôles d'events la possibilité d'appuyer sur `+` pour avoir un angle de plus ou moins 45° et `-` pour une vue de dessus visible dans la question 4b.

Pour ce qui est de faire tourner le plan, j'ai pu repérer que l'élément de réaffichage était la méthode `paintGL` de la classe `GLWidget`. À cet effet, j'ai implémenté à l'aide de librairie `QTimer` un contrôle du taux de rafraîchissement par seconde dans `void GLWidget::initializeGL()` tel que :

```c++
QTimer* timer = new QTimer();
connect(timer, SIGNAL(timeout()), this, SLOT(renderLater()));
timer->start(1000 / nbFPS);
```

On connecte le signal timeout() généré par le QTimer timer qui va appeler la méthode `renderLater()` autorisant l'update de la scène tel que :

```c++
void GLWidget::renderLater()
{
	update();
}
```

J'ai également ajouté un attribut `nbFPS` à la classe ``GLWidget` pour permettre de paramétrer le nombre de FPS désiré dans la scène, répondant ainsi en partie à la question 7. Par défaut, OpenGL est fixé à 60 FPS, il est ici paramétré de base à 30 FPS.

Pour jouer avec le nombre de FPS, on ajoute les 2 cases suivantes à notre méthode de gestion des événements claviers :

```c++
// Gestion des événements claviers pour les déplacements de la caméra
void GLWidget::keyPressEvent(QKeyEvent *event){
    switch(event->key()){
            ...
        case Qt::Key::Key_Up:
			nbFPS++;
			if (nbFPS > 100) nbFPS = 100;
			break;
		case Qt::Key::Key_Up:
			nbFPS--;
			if (nbFPS <= 0) nbFPS = 1;
			break;
    }
    update();
    event->accept();
}		

```



***



# Lexique :

* Déclaration des variables en dehors des fonctions :
	* **uniform** est un attribut qui va être distribué à l'ensemble des threads en lecture seule;
	* **attribute** : indique une variable par vertex, venant du programme OpenGL. Ne peut être placée que dans le vertex shader en lecture seule. Obsolète à partir de la version 150 du GLSL, remplacé par '*in*'.
	* **varying** : donnée en sortie du vertex shader en lecture et écriture, envoyée au fragment shader aprés avoir été interpolé (LECTURE UNIQUEMENT). Obsolète à partir de la version 150 du GLSL, remplacé par '*out*'.
	* **const** : indique une variable qui est constante (LECTURE UNIQUEMENT).
* Déclaration des variables dans les fonctions :
	* **in** : la variable est initialisée en entrée, mais n'est pas copié en retour (qualificatifs par défaut)
	* **out** : la variable est copiée en retour, mais non initialisée en entrée.
	* **inout** : la variable est initialisée en entrée, copiée en retour.
	* **const** : variable d'entrée constante.

# Webographie

*  https://doc.qt.io/qt-5/qtimer.html#singleShot-prop 
* https://thebookofshaders.com/
* https://openclassrooms.com/fr/courses/966823-developpez-vos-applications-3d-avec-opengl-3-3/960359-introduction
*  http://guillaume.belz.free.fr/doku.php?id=start 
*  https://perso.telecom-paristech.fr/elc/qt/Qt-Graphique.pdf 





