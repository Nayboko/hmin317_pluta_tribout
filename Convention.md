# Conventions

## Du nommage au sein du code source

Les classes qui sont le fruit de notre propre conception disposeront d'un préfixe `P` afin de pouvoir les différencier des autres classes pouvant avoir été importée. De même, on prendra soin d'ajouter les directives préprocesseurs usuelles dans les fichiers header.

```cpp
#ifndef PMACLASSE_HPP
#define PMACLASSE_HPP

class PMaClasse{
public :
    ...
};

#endif /* PMACLASSE_HPP */
```

****
En ce qui concerne le nommage des méthodes, nous suivrons la convention camelCase commune dans les langages tels que C++, JS, C# ou bien Java.

```cpp
class PMaClasse{
    public:
        void myFunction();
};

```


****

Afin de pouvoir repérer plus facilement le niveau d'accessibilité de la propriété d'une classe, on écrira son nom en minuscule pour signifier qu'elle est `private` ou `protected` et en majuscule si elle est `public`. De même, on ajoutera le préfixe `m_` pour signifier que la propriété est un membre de la classe courante.

```cpp
class PMaClasse{
    public:
        int M_ID;
        ...
    private:
        int m_counter;
        ...
};
```

**** 

Pour reconnaître qu'une propriétée est une collection ou un pointeur, on lui ajoutera un préfixe correspondant à son type.


```cpp
// .hpp
#include <vector>
#include <string>

class PMaClasse{
    private:
        int* m_p_number;
        std::vector<int> m_v_number;
        std::string m_s_name;
        ...
    public:
        int maFonction(); 
};

// .cpp

#include "PMaClasse.hpp"

int PMaClasse::maFonction(unsigned int pos){
    t_result = m_v_number.at(pos);
    // traitement sur t_result;
    return t_result;
}
```

## De l'écriture des méthodes, paramètres, propriétés et mot-clefs

Les propriétés doivent être au niveau d'accessibilité `private` et donc accompagnés des getters et setters correspondants en `public`. De même, les paramètres sont passés en `const` pour les paramètres des setters et le retour des getter

```cpp
class PMaClasse{
    private:
        std::string m_name;
    public;
        void setName(const std::string name);
        int getName()const;
};
```

****

Si les types de retour ou les paramètres d'une méthode sont des types complexes (tel que std::string) ou des objets, on peut les passer par référence selon les besoins.

```cpp
Object& getObject(){
    return m_object;
}

Object& returnCreatedObject(){
    return new Object();
}

bool modify_Object( Object& obj) {
    //  obj = myObj; return true; both possible
    return obj.modifySomething() == true;
}
```

****
