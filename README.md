# HMIN317_PLUTA_CALVET_BENDAKHLIA

Dépôt GIT pour l'UE HMIN317 : Moteur de jeux dans le cadre du M2 IMAGINA à la Faculté des Sciences de Montpellier.

## Objectif

Le moteur va devoir permettre de réaliser des jeux de type Rogue-Like avec une génération procédurale de terrain.

### Inspirations

* Into the Breach
* RimWorld
 
### Genre envisagés

* **Stratégie**
* **RPG Tactique (FF, Fallout)**
* RTS (Starcraft, Company of Heroes)
* Course
* Survie / Sandbox
* **Plateformer**
* **Hack'n Slash**
* **Rogue-Like**
* _FPS_ (en combinaison)
* Simulation

En gras, les genres préférés.

### Spécification

On gère **1 personnage** se déplaçant sur un ensemble de **tuiles** (voxel / grille) composant le terrain. Le terrain aura des **relief** et aura une **taille fixe 3D** (3 axes).

### Tour par tour ou Temps réel ?

Il va falloir réfléchir chacun de son côté pour déterminer quelle temporalité serait la plus adaptée. Il faut peser le pour et le contre. Réfléchir à ce qui est du côté technique et gameplay.

#### Tour par tour

* Machine à état
* Case par case
* Principalement adapté aux Tacticals

#### Temps réel

* Adapté à tous les genres